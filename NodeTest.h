/**
 * @file   NodeTest.h
 * @author Jim Daehn
 * @brief  TODO: Give brief description of this class.
 */

#ifndef NODETEST_H_
#define NODETEST_H_

#include <cppunit/extensions/HelperMacros.h>
#include "Node.h"

class NodeTest: public CPPUNIT_NS::TestFixture {

	CPPUNIT_TEST_SUITE(NodeTest);

	CPPUNIT_TEST(testDefaultNodeHasNoNextNode);
	CPPUNIT_TEST(testSingleParamNodeHasNoNextNode);
	CPPUNIT_TEST(testTwoParamNodeHasNonNullNextNode);

	CPPUNIT_TEST_SUITE_END();

public:
	NodeTest();
	virtual ~NodeTest();
	void setUp();
	void tearDown();

private:
	static const std::string MESSAGE;
	static const std::string COURSE_PREFIX;
	static const std::string COURSE_NAME;
	static const std::string UNIMPLEMENTED_TEST;

	void testDefaultNodeHasNoNextNode();
	void testSingleParamNodeHasNoNextNode();
	void testTwoParamNodeHasNonNullNextNode();

	Node<std::string>* defaultNode;
	Node<std::string>* coursePrefixNode;
	Node<std::string>* courseNameNode;
	Node<std::string>* itemNextNode;
};

// another way of creating a static const class data member
const std::string NodeTest::MESSAGE = "Expected value should equal actual value.";
const std::string NodeTest::COURSE_PREFIX = "CSC232";
const std::string NodeTest::COURSE_NAME = "Data Structures with C++";
const std::string NodeTest::UNIMPLEMENTED_TEST = "Unimplemented test.";

#endif /* NODETEST_H_ */
