/**
 * @file   LinkedBag.h
 * @author Jim Daehn
 * @brief  TODO: Give brief description of this class.
 */

#ifndef LINKED_BAG_H_
#define LINKED_BAG_H_

#include "BagInterface.h"
#include "Node.h"

/**
 * A link-based implementation of the ADT bag.
 */
template<typename ItemType>
class LinkedBag: public BagInterface<ItemType> {
public:
	LinkedBag();
	LinkedBag(const LinkedBag<ItemType>& aBag); // copy constructor

	virtual int getCurrentSize() const override;
	virtual bool isEmpty() const override;
	virtual bool add(const ItemType& newEntry) override;
	virtual bool remove(const ItemType& anEntry) override;
	virtual void clear() override;
	virtual bool contains(const ItemType& anEntry) const override;
	virtual int getFrequencyOf(const ItemType& anEntry) const override;
	virtual std::vector<ItemType> toVector() const override;

	// Lab 4
	virtual BagInterface<ItemType>* getUnionWithBag(BagInterface<ItemType>* aBag) const override;
	virtual BagInterface<ItemType>* getIntersectionWithBag(BagInterface<ItemType>* anotherBag) const override;
	virtual BagInterface<ItemType>* getDifferenceWithBag(BagInterface<ItemType>* anotherBag) const override;
	virtual ~LinkedBag();

private:
	Node<ItemType>* headPtr;
	int itemCount;

	/**
	 * Get a pointer to a node containing the given entry, or the null pointer if said entry does not exist in the bag.
	 *
	 * @param target the item for which we see a pointer
	 * @return A pointer to the given item is returned, or the null pointer if said item does not exist in the bag.
	 */
	Node<ItemType>* getPointerTo(const ItemType& target) const;
};

#include "LinkedBag.cpp"

#endif /* LINKED_BAG_H_ */
