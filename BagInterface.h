/**
 * @file   BagInterface.h
 * @author Jim Daehn
 * @brief  An abstract base class for a container class hierarchy developed in CSC232.
 */

#ifndef BAG_INTERFACE_H_
#define BAG_INTERFACE_H_

#include <vector>

template<typename ItemType>
class BagInterface {
public:
	/**
	 * Gets the current number of entries in this bag.
	 *
	 * @return The integer number of entries currently in the bag.
	 */
	virtual int getCurrentSize() const = 0;

	/**
	 * Determines whether this bag is empty.
	 *
	 * @return True if the bag is empty, false otherwise.
	 */
	virtual bool isEmpty() const = 0;

	/**
	 * Adds a new entry to this bag.
	 *
	 * @post   If successful, newEntry is stored in the bag and the count
	 *         of items in the bag has increased by 1.
	 *
	 * @param  newEntry The object to be added as a new entry.
	 *
	 * @return True if addition was successful, false otherwise.
	 */
	virtual bool add(const ItemType& newEntry) = 0;

	/**
	 * Removes one occurrence of a given entry from this bag, if possible.
	 *
	 * @post   If successful, anEntry has been removed from the bag and the
	 *         count of items in the bag has decreased by 1.
	 *
	 * @param  anEntry The entry to be removed.
	 *
	 * @return True if removal was successful, false otherwise.
	 */
	virtual bool remove(const ItemType& anEntry) = 0;

	/**
	 * Removes all entries from this bag.
	 *
	 * @post Bag contains no items, and the count of items is 0.
	 */
	virtual void clear() = 0;

	/**
	 * Counts the number of times a given entry appears in this bag.
	 *
	 * @param  anEntry The entry to be counted.
	 *
	 * @return The number of times anEntry appears in the bag.
	 */
	virtual int getFrequencyOf(const ItemType& anEntry) const = 0;

	/**
	 * Determines whether this bag contains a given entry.
	 *
	 * @param anEntry The entry to locate.
	 *
	 * @return True if this bag contains anEntry, false otherwise.
	 */
	virtual bool contains(const ItemType& anEntry) const = 0;

	/**
	 * Empties and then fills a given vector with all entries that are in
	 * this bag.
	 *
	 * @return A vector containing copies of all entries in this bag.
	 */
	virtual std::vector<ItemType> toVector() const = 0;

	/**
	 * Obtain a pointer to a bag that contains the union of this Bag with the given Bag.
	 *
	 * @param anotherBag the other Bag used to create the union
	 * @return A pointer to a bag containing the union of this Bag with given bag is returned.
	 */
	virtual BagInterface<ItemType>* getUnionWithBag(BagInterface<ItemType>* anotherBag) const = 0;

	/**
	 * Obtain a pointer to a bag that contains the intersection of this Bag with the given Bag.
	 *
	 * @param anotherBag the other Bag used to create the intersection
	 * @return A pointer to a bag containing the intersection of this Bag with given bag is returned.
	 */
	virtual BagInterface<ItemType>* getIntersectionWithBag(BagInterface<ItemType>* anotherBag) const = 0;

	/**
	 * Obtain a pointer to a bag that contains the difference of this Bag with the given Bag.
	 *
	 * @param anotherBag the other Bag used to create the difference
	 * @return A pointer to a bag containing the difference of this Bag with given bag is returned.
	 */
	virtual BagInterface<ItemType>* getDifferenceWithBag(BagInterface<ItemType>* anotherBag) const = 0;

	/**
	 * Destroys this bag and frees its assigned memory.
	 */
	virtual ~BagInterface() {
	}
};

#endif /* BAG_INTERFACE_H_ */
