/**
 * @file   LinkedBagTest.h
 * @author Jim Daehn
 * @brief  LinkedBag CPPUNIT specification.
 */

#ifndef LINKEDBAGTEST_H_
#define LINKEDBAGTEST_H_

#include <string>
#include <cppunit/extensions/HelperMacros.h>
#include "BagInterface.h"

class LinkedBagTest : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(LinkedBagTest);

		CPPUNIT_TEST(testUnionHasExpectedFrequencyOfA);
		CPPUNIT_TEST(testUnionHasExpectedFrequencyOfB);
		CPPUNIT_TEST(testUnionHasExpectedFrequencyOfC);
		CPPUNIT_TEST(testUnionHasExpectedFrequencyOfD);
		CPPUNIT_TEST(testUnionHasExpectedFrequencyOfE);

		CPPUNIT_TEST(testIntersectionHasExpectedFrequencyOfA);
		CPPUNIT_TEST(testIntersectionHasExpectedFrequencyOfB);
		CPPUNIT_TEST(testIntersectionHasExpectedFrequencyOfC);
		CPPUNIT_TEST(testIntersectionHasExpectedFrequencyOfD);
		CPPUNIT_TEST(testIntersectionHasExpectedFrequencyOfE);

		CPPUNIT_TEST(testDifferenceHasExpectedFrequencyOfA);
		CPPUNIT_TEST(testDifferenceHasExpectedFrequencyOfB);
		CPPUNIT_TEST(testDifferenceHasExpectedFrequencyOfC);
		CPPUNIT_TEST(testDifferenceHasExpectedFrequencyOfD);
		CPPUNIT_TEST(testDifferenceHasExpectedFrequencyOfE);

	CPPUNIT_TEST_SUITE_END();

public:
    LinkedBagTest();
    virtual ~LinkedBagTest();
    void setUp();
    void tearDown();

private:
    const std::string ASSERTION_MESSAGE = "Actual should equal expected.";
    void testUnionHasExpectedFrequencyOfA();
    void testUnionHasExpectedFrequencyOfB();
    void testUnionHasExpectedFrequencyOfC();
    void testUnionHasExpectedFrequencyOfD();
    void testUnionHasExpectedFrequencyOfE();

    void testIntersectionHasExpectedFrequencyOfA();
    void testIntersectionHasExpectedFrequencyOfB();
    void testIntersectionHasExpectedFrequencyOfC();
    void testIntersectionHasExpectedFrequencyOfD();
    void testIntersectionHasExpectedFrequencyOfE();

    void testDifferenceHasExpectedFrequencyOfA();
    void testDifferenceHasExpectedFrequencyOfB();
    void testDifferenceHasExpectedFrequencyOfC();
    void testDifferenceHasExpectedFrequencyOfD();
    void testDifferenceHasExpectedFrequencyOfE();

    BagInterface<std::string>* bag;
    BagInterface<std::string>* anotherBag;
    BagInterface<std::string>* expectedBag;
    BagInterface<std::string>* actualBag;
};

#endif /* LINKEDBAGTEST_H_ */
