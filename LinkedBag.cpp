/**
 * @file   LinkedBag.cpp
 * @author McKenzie Riley, Morgan Lockhart
 * @brief  TODO: Give brief description of this class.
 */

#include "LinkedBag.h"

// Core "method"
template<typename ItemType>
LinkedBag<ItemType>::LinkedBag() :
		headPtr(nullptr), itemCount(0) {
	// intentionally blank
}

template<typename ItemType>
LinkedBag<ItemType>::LinkedBag(const LinkedBag<ItemType>& aBag) {
	itemCount = aBag.itemCount;
	Node<ItemType>* origChainPtr = aBag.headPtr;
	if (origChainPtr == nullptr) {
		headPtr = nullptr; // Original bag is empty; so is copy
	} else {
		// Copy first node
		headPtr = new Node<ItemType>();
		headPtr->setItem(origChainPtr->getItem());

		// Copy remaining nodes
		Node<ItemType>* newChainPtr = headPtr; // Last-node pointer
		origChainPtr = origChainPtr->getNext(); // Advance pointer
		while (origChainPtr != nullptr) {
			// Get next item from original chain
			ItemType nextItem = origChainPtr->getItem();

			// Create a new node containing next item
			Node<ItemType>* newNodePtr = new Node<ItemType>(nextItem);

			// Link new node to end of new chain
			newChainPtr->setNext(newNodePtr);

			// Advance pointers
			newChainPtr = newChainPtr->getNext();
			origChainPtr = origChainPtr->getNext();
		}
		newChainPtr->setNext(nullptr);
	}
}

template<typename ItemType>
LinkedBag<ItemType>::~LinkedBag() {
	if (headPtr != nullptr) {
		delete headPtr;
		headPtr = nullptr;
	}
}

// Core method
template<typename ItemType>
bool LinkedBag<ItemType>::add(const ItemType& newEntry) {
	// Most convenient location to add a new node is at the beginning of the chain
	// because the first node is the only one we can access *directly*.

	// Create a new node for the entry
	Node<ItemType>* newNodePtr = new Node<ItemType>();

	// Store the entry in this new node
	newNodePtr->setItem(newEntry);

	// The next two lines effectively insert the node in the beginning of the chain
	// The ORDER of the next two lines is important! Sketch it out to see why...
	newNodePtr->setNext(headPtr);
	headPtr = newNodePtr;

	// Bookkeeping
	++itemCount;

	// The method is always successful
	return true;
}

// Core method
template<class ItemType>
std::vector<ItemType> LinkedBag<ItemType>::toVector() const {
	std::vector<ItemType> bagContents;
	Node<ItemType>* curPtr = headPtr;
	int counter { 0 };

	// NOTE: It's best to compare curPtr with nullptr rather than comparing curPtr->getNext() with nullptr
	// because we would end up not accessing the last node; also think about what curPtr->next() means for
	// an empty bag
	while ((curPtr != nullptr) && (counter < itemCount)) {
		bagContents.push_back(curPtr->getItem());
		curPtr = curPtr->getNext();
		++counter;
	}
	return bagContents;
}

// Core method
template<class ItemType>
int LinkedBag<ItemType>::getCurrentSize() const {
	return itemCount;
}

template<class ItemType>
bool LinkedBag<ItemType>::isEmpty() const {
	return itemCount == 0;
}

/********** Additional Methods **********/

template<class ItemType>
int LinkedBag<ItemType>::getFrequencyOf(const ItemType& anEntry) const {
	int frequency { 0 };
	int counter { 0 };
	Node<ItemType>* curPtr = headPtr;
	while ((curPtr != nullptr) && (counter < itemCount)) {
		if (anEntry == curPtr->getItem()) {
			++frequency;
		}
		++counter;
		curPtr = curPtr->getNext();
	}

	return frequency;
}

template<typename ItemType>
Node<ItemType>* LinkedBag<ItemType>::getPointerTo(const ItemType& target) const {
	bool found { false };
	Node<ItemType>* curPtr = headPtr;
	while (!found && (curPtr != nullptr)) {
		if (target == curPtr->getItem()) {
			found = true;
		} else {
			curPtr = curPtr->getNext();
		}
	}
	return curPtr;
}

template<class ItemType>
bool LinkedBag<ItemType>::contains(const ItemType& anEntry) const {
	return (getPointerTo(anEntry) != nullptr);
}

/*
 * remove(anEntry)
 *    Find the node that contains anEntyr
 *    Replace anEntry with the entry that is in the first node
 *    Delete the first node
 */
template<class ItemType>
bool LinkedBag<ItemType>::remove(const ItemType& anEntry) {
	Node<ItemType>* entryNodePtr = getPointerTo(anEntry);
	bool canRemoveItem { !isEmpty() && (entryNodePtr != nullptr) };
	if (canRemoveItem) {
		// Copy data from first node to located node
		entryNodePtr->setItem(headPtr->getItem());

		// Disconnect first node
		Node<ItemType>* nodeToDeletePtr = headPtr;
		headPtr = headPtr->getNext();

		// Return node to the system
		nodeToDeletePtr->setNext(nullptr);
		delete nodeToDeletePtr;
		nodeToDeletePtr = nullptr;

		--itemCount;
	}

	return canRemoveItem;
}

template<class ItemType>
void LinkedBag<ItemType>::clear() {
	Node<ItemType>* nodeToDeletePtr = headPtr;
	while (headPtr != nullptr) {
		headPtr = headPtr->getNext();

		// Return the node to the system
		nodeToDeletePtr->setNext(nullptr);
		delete nodeToDeletePtr;
		nodeToDeletePtr = headPtr;
	} // at this point, headPtr is nullptr; nodeToDeletePtr is nullptr

	itemCount = 0;
}

/********** Lab 4 *********/
template<class ItemType>
BagInterface<ItemType>* LinkedBag<ItemType>::getUnionWithBag(BagInterface<ItemType>* anotherBag) const {
	LinkedBag<ItemType>* bag = new LinkedBag<ItemType>();
	Node<ItemType>* curPtr = headPtr;

 	LinkedBag<ItemType>* thatBag = dynamic_cast<LinkedBag<ItemType>*>(anotherBag);
	while (curPtr != nullptr) {
		
 	
		bag->add(curPtr->getItem());
		curPtr = curPtr->getNext(); // Move to the next element
		
	}
	
	Node<ItemType>*othercurPtr = thatBag->headPtr;
	while (curPtr != nullptr) {
		
		
		thatBag->add(curPtr->getItem());
 		curPtr = curPtr->getNext(); // Move to the next element
		
 	}
	return bag;
	
	
}

template<class ItemType>
BagInterface<ItemType>* LinkedBag<ItemType>::getIntersectionWithBag(BagInterface<ItemType>* anotherBag) const {
	LinkedBag<ItemType>* bag = new LinkedBag<ItemType>();
	Node<ItemType>* curPtr = headPtr;

 	// TODO: Implement me.

 	LinkedBag<ItemType>* thatBag = dynamic_cast<LinkedBag<ItemType>*>(anotherBag);
	while (curPtr != nullptr) {
	
		Node<ItemType>*othercurPtr = thatBag->headPtr;
		if (curPtr->getItem() == othercurPtr->getItem())
		{
			bag->add(curPtr->getItem());
		}
	
	}

	return bag;
}
	
	

	return bag;
}

template<class ItemType>
BagInterface<ItemType>* LinkedBag<ItemType>::getDifferenceWithBag(BagInterface<ItemType>* anotherBag) const {
	LinkedBag<ItemType>* bag = new LinkedBag<ItemType>();
	Node<ItemType>* curPtr = headPtr;

 	// TODO: Implement me.

 	LinkedBag<ItemType>* thatBag = dynamic_cast<LinkedBag<ItemType>*>(anotherBag);
	while (curPtr != nullptr) {
	
		Node<ItemType>*othercurPtr = thatBag->headPtr;
		if (curPtr->getItem() == othercurPtr->getItem())
		{
			bag->remove(othercurPtr->getItem());
		}
		else
		{
			bag->add(curPtr->getItem());
		}
	
	}
	return bag;

}
